package plane_mission_project.mission_types_on_aircrafts;

import plane_mission_project.capabilities.AttackCapability;

public interface Attacking {
    void setAttackingCapabilities(AttackCapability attackingCapabilities);
}
