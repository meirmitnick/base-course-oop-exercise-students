package plane_mission_project.mission_types_on_aircrafts;

import plane_mission_project.capabilities.IntelligenceCapability;

public interface Intelligence {
    void setIntelligenceCapability(IntelligenceCapability intelligenceCapability);
}
