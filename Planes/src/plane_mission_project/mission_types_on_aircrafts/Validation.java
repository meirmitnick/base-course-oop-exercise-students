package plane_mission_project.mission_types_on_aircrafts;

import plane_mission_project.capabilities.ValidationCapability;

public interface Validation {
     void setValidationCapabilities(ValidationCapability validationCapability);
}
