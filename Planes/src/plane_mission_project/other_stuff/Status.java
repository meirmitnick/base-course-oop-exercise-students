package plane_mission_project.other_stuff;

public enum Status {
    IN_THE_AIR,
    READY,
    NOT_READY
}
