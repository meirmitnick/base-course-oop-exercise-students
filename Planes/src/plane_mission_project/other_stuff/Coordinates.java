package plane_mission_project.other_stuff;

public class Coordinates {
    private  float x;
    private  float y;

    public Coordinates(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "Coordinates: " + this.x + ", " + this.y;
    }
}
