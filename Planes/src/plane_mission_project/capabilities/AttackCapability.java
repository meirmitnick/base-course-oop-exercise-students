package plane_mission_project.capabilities;

import plane_mission_project.other_stuff.TypeOfMissle;

public class AttackCapability {
    private int amountOfMissles;
    private TypeOfMissle typeOfMissle;

    public int getAmountOfMissles() {
        return this.amountOfMissles;
    }

    public void setAmountOfMissles(int amountOfMissles) {
        this.amountOfMissles = amountOfMissles;
    }

    public TypeOfMissle getTypeOfMissle() {
        return this.typeOfMissle;
    }

    public void setTypeOfMissle(TypeOfMissle typeOfMissle) {
        this.typeOfMissle = typeOfMissle;
    }

    public AttackCapability(int amountOfMissles, TypeOfMissle typeOfMissle) {
        this.amountOfMissles = amountOfMissles;
        this.typeOfMissle = typeOfMissle;
    }
}
