package plane_mission_project.capabilities;

import plane_mission_project.other_stuff.Sensors;

public class IntelligenceCapability {
    private Sensors sensor;

    public IntelligenceCapability(Sensors sensor) {
        this.sensor = sensor;
    }

    public Sensors getSensor() {
        return this.sensor;
    }

    public void setSensor(Sensors sensor) {
        this.sensor = sensor;
    }
}
