package plane_mission_project.capabilities;

import plane_mission_project.other_stuff.Cameras;

public class ValidationCapability {
    private Cameras camera;

    public ValidationCapability(Cameras camera) {
        this.camera= camera;
    }

    public Cameras getCameras() {
        return camera;
    }

    public void setCameras(Cameras cameras) {
        this.camera = cameras;
    }
}
