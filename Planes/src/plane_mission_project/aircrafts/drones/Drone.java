package plane_mission_project.aircrafts.drones;

import plane_mission_project.aircrafts.Aircraft;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;

public abstract class Drone extends Aircraft {
    public Drone() {
    }

    public Drone(int hoursSinceLastRepair, Status status, int maxHoursWithoutRepair, Coordinates base) {
        super(hoursSinceLastRepair, status, maxHoursWithoutRepair, base);
    }

    public void hoverOverLocation(Coordinates coordinates){
    this.setStatus(Status.IN_THE_AIR);
    System.out.println("Hovering over"+ coordinates.toString());
}
}
