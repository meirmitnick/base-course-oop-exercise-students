package plane_mission_project.aircrafts.drones.haron;

import plane_mission_project.aircrafts.drones.Drone;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;

public abstract class Haron extends Drone {
    private static final int MAX_HOURS_FOR_HARON = 150;

    public Haron() {
    }

    public Haron(int hoursSinceLastRepair, Status status, Coordinates base) {
        super(hoursSinceLastRepair, status, MAX_HOURS_FOR_HARON, base);
    }
}
