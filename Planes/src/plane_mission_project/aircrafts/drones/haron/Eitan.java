package plane_mission_project.aircrafts.drones.haron;

import plane_mission_project.mission_types_on_aircrafts.Attacking;
import plane_mission_project.mission_types_on_aircrafts.Intelligence;
import plane_mission_project.capabilities.AttackCapability;
import plane_mission_project.capabilities.IntelligenceCapability;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;


public class Eitan extends Haron implements Attacking, Intelligence {

    public Eitan(int hoursSinceLastRepair, Status status, Coordinates base, AttackCapability attackCapability, IntelligenceCapability intelligenceCapability) {
        super(hoursSinceLastRepair, status, base);
        this.setAttackingCapabilities(attackCapability);
        this.setIntelligenceCapability(intelligenceCapability);
    }

    @Override
    public void setAttackingCapabilities(AttackCapability attackingCapabilities) {
        this.attackCapability = attackingCapabilities;
    }

    @Override
    public void setIntelligenceCapability(IntelligenceCapability intelligenceCapability) {
        this.intelligenceCapability = intelligenceCapability;
    }
}
