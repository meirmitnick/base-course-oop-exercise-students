package plane_mission_project.aircrafts.drones.hermon;

import plane_mission_project.aircrafts.drones.Drone;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;

public abstract class Hermon extends Drone {
    private static final int MAX_HOURS_FOR_HERMON = 100;

    public Hermon() {
    }

    public Hermon(int hoursSinceLastRepair, Status status, Coordinates base) {
        super(hoursSinceLastRepair, status, MAX_HOURS_FOR_HERMON, base);
    }
}
