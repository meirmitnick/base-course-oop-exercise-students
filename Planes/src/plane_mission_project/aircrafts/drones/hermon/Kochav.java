package plane_mission_project.aircrafts.drones.hermon;

import plane_mission_project.mission_types_on_aircrafts.Attacking;
import plane_mission_project.mission_types_on_aircrafts.Intelligence;
import plane_mission_project.mission_types_on_aircrafts.Validation;
import plane_mission_project.capabilities.AttackCapability;
import plane_mission_project.capabilities.IntelligenceCapability;
import plane_mission_project.capabilities.ValidationCapability;
import plane_mission_project.other_stuff.*;

public class Kochav extends Hermon implements Attacking, Intelligence, Validation {

    public Kochav(int hoursSinceLastRepair, Status status, Coordinates base, AttackCapability attackCapability, ValidationCapability validationCapability, IntelligenceCapability intelligenceCapability) {
        super(hoursSinceLastRepair, status, base);
        setAttackingCapabilities(attackCapability);
        setIntelligenceCapability(intelligenceCapability);
        setValidationCapabilities(validationCapability);
    }

    @Override
    public void setAttackingCapabilities(AttackCapability attackingCapabilities) {
        this.attackCapability = attackingCapabilities;
    }

    @Override
    public void setIntelligenceCapability(IntelligenceCapability intelligenceCapability) {
        this.intelligenceCapability = intelligenceCapability;
    }

    @Override
    public void setValidationCapabilities(ValidationCapability validationCapability) {
        this.validationCapability = validationCapability;
    }
}
