package plane_mission_project.aircrafts.drones.hermon;

import plane_mission_project.mission_types_on_aircrafts.Intelligence;
import plane_mission_project.mission_types_on_aircrafts.Validation;
import plane_mission_project.capabilities.IntelligenceCapability;
import plane_mission_project.capabilities.ValidationCapability;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;

public class Zik extends Hermon implements Intelligence, Validation {

    public Zik(int hoursSinceLastRepair, Status status, Coordinates base, IntelligenceCapability intelligenceCapability, ValidationCapability validationCapability) {
        super(hoursSinceLastRepair, status, base);
        this.setIntelligenceCapability(intelligenceCapability);
        this.setValidationCapabilities(validationCapability);
    }

    @Override
    public void setIntelligenceCapability(IntelligenceCapability intelligenceCapability) {
        this.intelligenceCapability = intelligenceCapability;
    }

    @Override
    public void setValidationCapabilities(ValidationCapability validationCapability) {
        this.validationCapability = validationCapability;
    }
}
