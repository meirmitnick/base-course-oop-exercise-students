package plane_mission_project.aircrafts.jets;

import plane_mission_project.aircrafts.Aircraft;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;

public abstract class FighterJet extends Aircraft {
    private static final int MAX_HOURS_FOR_JET = 250;

    public FighterJet() {
    }

    public FighterJet(int hoursSinceLastRepair, Status status, Coordinates base) {
        super(hoursSinceLastRepair, status, MAX_HOURS_FOR_JET, base);
    }
}
