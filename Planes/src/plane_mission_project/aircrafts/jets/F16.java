package plane_mission_project.aircrafts.jets;

import plane_mission_project.mission_types_on_aircrafts.Attacking;
import plane_mission_project.mission_types_on_aircrafts.Validation;
import plane_mission_project.capabilities.AttackCapability;
import plane_mission_project.capabilities.ValidationCapability;
import plane_mission_project.other_stuff.Coordinates;
import plane_mission_project.other_stuff.Status;


public class F16 extends FighterJet implements Attacking, Validation {
    public F16() {
    }

    public F16(int hoursSinceLastRepair, Status status, Coordinates base, AttackCapability attackCapability, ValidationCapability validationCapability) {
        super(hoursSinceLastRepair, status, base);
        this.setAttackingCapabilities(attackCapability);
        this.setValidationCapabilities(validationCapability);

    }

    @Override
    public void setAttackingCapabilities(AttackCapability attackingCapabilities) {
        this.attackCapability = attackingCapabilities;
    }

    @Override
    public void setValidationCapabilities(ValidationCapability validationCapability) {
        this.validationCapability = validationCapability;
    }
}
