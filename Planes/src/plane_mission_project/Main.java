package plane_mission_project;

import plane_mission_project.aircrafts.drones.Drone;
import plane_mission_project.aircrafts.drones.hermon.Kochav;
import plane_mission_project.aircrafts.jets.F15;
import plane_mission_project.aircrafts.jets.F16;
import plane_mission_project.capabilities.AttackCapability;
import plane_mission_project.capabilities.IntelligenceCapability;
import plane_mission_project.capabilities.ValidationCapability;
import plane_mission_project.missions.AttackMission;
import plane_mission_project.missions.IntelligenceMission;
import plane_mission_project.missions.ValidationMission;
import plane_mission_project.other_stuff.*;

public class Main {

    public static void main(String[] args) {
        intelligenceMission();
        attackMission();
        validationMission();
    }

    private static void intelligenceMission() {
        Coordinates coordinates = new Coordinates(33.2033427805222f, 44.5176910494946f);
        Kochav kochav = new Kochav(15, Status.READY, new Coordinates(31.82706f, 34.81739f), new AttackCapability(4, TypeOfMissle.PYTHON), null, new IntelligenceCapability(Sensors.INFRA_RED));
        IntelligenceMission intelligenceMission = new IntelligenceMission("Iraq", coordinates, "Dror zalicha", kochav);

        intelligenceMission.begin();
        ((Drone) intelligenceMission.getAircraft()).hoverOverLocation(intelligenceMission.getCoordinates());
        intelligenceMission.finish();
    }


    private static void attackMission() {
        Coordinates coordinates = new Coordinates(33.20334f, 44.51769f);
        F15 f15 = new F15(15, Status.READY, new Coordinates(31.8276f, 34.817f), new AttackCapability(250, TypeOfMissle.SPICE250), null);
        AttackMission attackMission = new AttackMission("Tuwaitha Nuclear Research Center", coordinates, "Ze'ev Raz", f15);
        attackMission.begin();
        attackMission.finish();
    }

    private static void validationMission() {
        Coordinates coordinates = new Coordinates(33.20334f, 44.51769f);
        F16 f16 = new F16(15, Status.READY, new Coordinates(31.8276f, 34.817f), new AttackCapability(250, TypeOfMissle.SPICE250), new ValidationCapability(Cameras.THERMAL));
        ValidationMission validationMission = new ValidationMission("Tuwaitha Nuclear Research Center", coordinates, "Ilan Ramon", f16);
        validationMission.begin();
        validationMission.finish();
    }

}
