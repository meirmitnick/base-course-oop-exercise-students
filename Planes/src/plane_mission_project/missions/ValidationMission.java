package plane_mission_project.missions;

import plane_mission_project.aircrafts.Aircraft;
import plane_mission_project.mission_types_on_aircrafts.Validation;
import plane_mission_project.other_stuff.Coordinates;

public class ValidationMission extends Mission {

    private String objective;

    public ValidationMission(String objective, Coordinates coordinates, String pilot, Validation aircraft) throws ClassCastException {
        super(coordinates, pilot, (Aircraft) aircraft);
        this.objective = objective;
    }

    @Override
    public String executeMission() {
        System.out.println(this.getPilot() + ": " + this.getAircraft().getClass().getSimpleName() + " Is taking pictures of " + this.getObjective() + "with : " + this.getAircraft().getValidationCapability().getCameras());
        return "";
    }

    private String getObjective() {
        return this.objective;
    }
}
