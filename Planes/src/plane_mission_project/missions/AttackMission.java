package plane_mission_project.missions;

import plane_mission_project.aircrafts.Aircraft;
import plane_mission_project.mission_types_on_aircrafts.Attacking;
import plane_mission_project.other_stuff.Coordinates;

public class AttackMission extends Mission {

    private final String target;

    public AttackMission(String target, Coordinates coords, String pilot, Attacking aircraft) throws ClassCastException{
        super(coords, pilot, (Aircraft) aircraft);
        this.target = target;
    }

    @Override
    public String executeMission() {
        System.out.println(this.getPilot() + ": " + this.getAircraft().getClass().getSimpleName() + " Attacking " + this.target + " with " + this.getAircraft().getAttackCapability().getTypeOfMissle());
        return "";
    }

}
