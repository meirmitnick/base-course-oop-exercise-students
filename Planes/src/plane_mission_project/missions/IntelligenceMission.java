package plane_mission_project.missions;

import plane_mission_project.aircrafts.Aircraft;
import plane_mission_project.mission_types_on_aircrafts.Intelligence;
import plane_mission_project.other_stuff.Coordinates;

public class IntelligenceMission extends Mission {

   private final String region;

    public IntelligenceMission(String region, Coordinates coordinates, String pilot, Intelligence aircraft)  throws ClassCastException {
        super(coordinates, pilot, (Aircraft) aircraft);
        this.region = region;
    }


    @Override
    public String executeMission() {
        System.out.println(this.getPilot() + ": " + this.getAircraft().getClass().getSimpleName() + " Is collecting data in " + this.getRegion() + " with : " + this.getAircraft().getIntelligenceCapability().getSensor());
        return "";
    }

    private String getRegion() {
        return this.region;
    }
}
